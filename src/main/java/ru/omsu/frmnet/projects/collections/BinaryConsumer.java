package ru.omsu.frmnet.projects.collections;


@FunctionalInterface
public interface BinaryConsumer<T,V> {

    void accept(T t,V v);
}
