package ru.omsu.frmnet.projects.collections;

@FunctionalInterface
public interface BinaryFunction<T, V, R> {

    R apply(T t, V v);

}
