package ru.omsu.frmnet.projects.collections;

import ru.omsu.frmnet.projects.nio.Trainee;
import ru.omsu.frmnet.projects.nio.TrainingException;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;

public class CollectionService {

    public static final Consumer<List<?>> FLIP_LIST =
            Collections::reverse;

    public static final BinaryConsumer<List<?>, Integer> SHIFT_ELEMS =
            Collections::rotate;

    public static final Consumer<List<?>> SHUFFLE_LIST =
            Collections::shuffle;

    public static final Function<List<Trainee>, Trainee> FIND_MAX_RATING_TRAINEES =
            list -> list.stream()
                        .max(Comparator.comparingInt(Trainee::getRating))
                        .orElse(null);

    public static final Consumer<List<Trainee>> SORTING_BY_RATING =
            list -> list.sort(Comparator.comparingInt(Trainee::getRating));

    public static final Consumer<List<Trainee>> SORTING_BY_NAME =
            list -> list.sort(Comparator.comparing(Trainee::getFirstName));

    public static final BinaryFunction<List<Trainee>, String, Trainee> FIND_TRAINEE_BY_NAME =
            (list, nm) -> list.stream()
                               .filter(trn -> trn.getFirstName().equals(nm))
                               .findFirst()
                               .orElse(null);


    private static final int ELEM_COUNT = 100000;
    private static final int FIND_TIMES = 100000;

    private static void fillList(List<Integer> list) {
        for(int i = 0; i < ELEM_COUNT; i++)
            list.add(i);
    }

    private static void fillListRandom(List<Integer> list) {
        Random random = new Random();

        for(int i = 0; i < ELEM_COUNT; i++)
            list.add(random.nextInt());
    }

    private static void fillSetRandom(Set<Integer> set) {
        for(int i = 0; i < ELEM_COUNT; i++)
            set.add(i);
    }

    private static void checkTimeV1(List<Integer> list) {
        Random random = new Random();

        for(int i = 0; i < FIND_TIMES; i++)
            list.get(random.nextInt(ELEM_COUNT));
    }

    private static void checkTimeV2(Collection<Integer> collection){
        Random random = new Random();

        for(int i = 0; i < FIND_TIMES; i++)
            collection.contains(random.nextInt());
    }




    public static void main(String[] args) throws TrainingException {
        List<Trainee> trainees = Collections.synchronizedList(new ArrayList<>());
        trainees.add(new Trainee("adsada", "asdad", 5));
        trainees.add(new Trainee("Sasha", "Kovalev", 2));
        trainees.add(new Trainee("Zxzczczx", "affffd", 5));

        FLIP_LIST.accept(trainees);
        SHIFT_ELEMS.accept(trainees, 2);
        SHUFFLE_LIST.accept(trainees);

        Trainee trainee = FIND_MAX_RATING_TRAINEES.apply(trainees);
        SORTING_BY_RATING.accept(trainees);
        SORTING_BY_NAME.accept(trainees);
        Trainee trainee1 = FIND_TRAINEE_BY_NAME.apply(trainees, "Sasha");

        System.out.println(trainee + " " + trainee1);


        List<Integer> arrayList = Collections.synchronizedList(new ArrayList<>());
        List<Integer> linkedList = Collections.synchronizedList( new LinkedList<>());

        fillList(arrayList);
        fillList(linkedList);


        long start = System.nanoTime();
        checkTimeV1(arrayList);
        long end = System.nanoTime() - start;


        start = System.nanoTime();
        checkTimeV1(linkedList);
        end = System.nanoTime() - start;

        Set<Integer> hashSet = Collections.synchronizedSet(new HashSet<>());
        Set<Integer> treeSet = Collections.synchronizedSet(new TreeSet<>());
        List<Integer> arrayList2 = Collections.synchronizedList(new ArrayList<>());

        fillListRandom(arrayList2);
        fillSetRandom(hashSet);
        fillSetRandom(treeSet);



        start = System.nanoTime();
        checkTimeV2(hashSet);
        end = System.nanoTime() - start;


        start = System.nanoTime();
        checkTimeV2(treeSet);
        end = System.nanoTime() - start;


        start = System.nanoTime();
        checkTimeV2(arrayList2);
        end = System.nanoTime() - start;


        System.out.println("\n\n\n");

        /*
        * BIT SET
        * */
        BitSet bitSet = new BitSet();
        bitSet.set(16);
        bitSet.get(16);
        bitSet.clear(16);



        Set<Integer> hashSet1 =  Collections.synchronizedSet(new HashSet<>());
        Set<Integer> treeSet1 = Collections.synchronizedSet(new TreeSet<>());

        start = System.nanoTime();
        for(int i = 0; i < 1000000; i++)
            bitSet.set(i);
        end = System.nanoTime() - start;


        start = System.nanoTime();
        for(int i = 0; i < 1000000; i++)
            hashSet1.add(i);
        end = System.nanoTime() - start;


        start = System.nanoTime();
        for(int i = 0; i < 1000000; i++)
            treeSet1.add(i);
        end = System.nanoTime() - start;



        EnumSet<Color> enumSet = EnumSet.allOf(Color.class);
        EnumSet<Color> enumSet1 = EnumSet.of(Color.WHITE);
        EnumSet<Color> enumSet2 = EnumSet.range(Color.RED, Color.GREEN);
        EnumSet<Color> enumSet3 = EnumSet.noneOf(Color.class);


        System.out.println(enumSet3.isEmpty() + "PUSTO");
    }
}
