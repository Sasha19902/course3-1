package ru.omsu.frmnet.projects.collections;

public enum Color {
    BLACK, RED, BLUE, GREEN, WHITE
}
