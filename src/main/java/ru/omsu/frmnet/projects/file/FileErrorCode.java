package ru.omsu.frmnet.projects.file;

public enum FileErrorCode {
    FILE_NOT_CREATED("File create exception"),
    FILE_NOT_DELETED("File delete exception"),
    FILE_NOT_FOUND("File not found"),
    FILE_NOT_DIRECTORY("File not directory"),
    DIRECTORY_NOT_CREATED("File create exception");

    private String errorMessage;

    FileErrorCode(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
