package ru.omsu.frmnet.projects.file;

public class FileException extends Exception {

    private FileErrorCode fileErrorCode;

    public FileException(FileErrorCode fileErrorCode) {
        super();
        this.fileErrorCode = fileErrorCode;
    }

    public FileException() {
        super();
    }

    public FileException(String message) {
        super(message);
    }

    public FileException(String message, Throwable cause) {
        super(message, cause);
    }

    public FileException(Throwable cause) {
        super(cause);
    }


    public FileErrorCode getFileErrorCode() {
        return fileErrorCode;
    }
}
