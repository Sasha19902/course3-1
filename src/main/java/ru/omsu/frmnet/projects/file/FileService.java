package ru.omsu.frmnet.projects.file;

import ru.omsu.frmnet.projects.nio.Trainee;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class FileService {

    public static void  writeTraineeToTextFileOneLine(File file, Trainee trainee) throws IOException {
        try(OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(file), "UTF-8")){
            outputStreamWriter.write(trainee.getFullName() + " " + trainee.getRating());
        }
    }

    public static void createDirectory(String path) throws FileException {
        if(!new File(path).mkdir())
            throw new FileException(FileErrorCode.DIRECTORY_NOT_CREATED);
    }

    public static void createFile(String path) throws FileException, IOException {
        if(!new File(path).createNewFile())
            throw new FileException(FileErrorCode.FILE_NOT_CREATED);
    }

    public static void deleteFile(String path) throws FileException {
        if(!new File(path).delete())
            throw new FileException(FileErrorCode.FILE_NOT_DELETED);
    }

    public static void renameTo(String path, String newName) throws FileException {
        if (!new File(path).renameTo(new File(newName)))
            throw new FileException(FileErrorCode.FILE_NOT_DELETED);
    }

    public static String getFullName(String fileName) throws FileException {
        File file = new File(fileName);

        if(!file.exists())
            throw new FileException(FileErrorCode.FILE_NOT_DELETED);

        return file.getAbsolutePath();
    }

    public static boolean isFile(String path) {
        return new File(path).isFile();
    }

    public static boolean isDirectory(String path) {
        return new File(path).isDirectory();
    }

    /*
     * If mask is empty or null return all files in directory
     * */

    public static List<String> getFilesInDirectory(String path, String mask) throws FileException {
        List<String> result = null;
        File file = new File(path);

        if(Objects.nonNull(file.listFiles())) {

            if(Objects.isNull(mask) || mask.isEmpty()) {
                result = Arrays.stream((Objects.requireNonNull(file.listFiles())))
                               .map(File::getName)
                               .collect(Collectors.toList());
            }
            else {
                result = Arrays.stream(Objects.requireNonNull(file.listFiles((dir, name) -> name.contains(mask))))
                               .map(File::getName)
                               .collect(Collectors.toList());
            }
        }
        return result;
    }

    public static void main(String[] args) {

    }
}
