package ru.omsu.frmnet.projects.functional;


import java.util.*;
import java.util.function.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


public class FunctionalService {

    public static final Function<String, List<String>> SPLIT_BY_SPACE =
            str -> Arrays.asList(str.split(" "));


    public static final Function<List<?>, Integer> COUNT_ELEM =
            List::size;

    public static final Function<String, Integer> SPLIT_AND_COUNT_V1 =
            SPLIT_BY_SPACE.andThen(COUNT_ELEM);

    public static final Function<String, Integer> SPLIT_AND_COUNT_V2 =
            COUNT_ELEM.compose(SPLIT_BY_SPACE);

    public static final Function<String, Person> CREATE =
            Person::new;

    public static final BinaryOperator<Double> MAX =
            Math::max;

    public static final Supplier<Date> GET_CURRENT_DATE =
            Date::new;

    public static final Predicate<Integer> IS_EVEN =
            num -> num % 2 == 0;


    public static final BiPredicate<Integer, Integer> ARE_EQUAL =
            Integer::equals;

    private static final int THIRTY_OLD = 30;



    public static final Function<List<Person>, List<String>> UNIC_NAME_OLD_THIRTY =
            list -> list.stream()
                        .filter(prsn -> prsn.getAge() > THIRTY_OLD)
                        .map(Person::getName)
                        .distinct()
                        .sorted(Comparator.comparing(String::length))
                        .collect(Collectors.toList());



    public static final Function<List<Person>, Map<String, List<Person>>> UNIC_NAME_OLD_THIRTY_GROUPING_BY =
            list -> list.stream()
                        .filter(prsn -> prsn.getAge() > THIRTY_OLD)
                        .collect(Collectors.groupingBy(Person::getName))
                        .entrySet()
                        .stream()
                        .sorted(Comparator.comparingInt(entry -> entry.getValue().size()))
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2, LinkedHashMap::new));


    public static IntStream transform(IntStream intStream, IntUnaryOperator op) {
        return intStream.map(op);
    }

    public static IntStream transformAndParallel(IntStream intStream, IntUnaryOperator op) {
        return transform(intStream, op).parallel();
    }

    public static final Function<List<Integer>, Integer> SUM_ALL =
            list -> list.stream()
                        .reduce((x,y) -> x + y)
                        .orElse(0);

    public static final Function<List<Integer>, Integer> MULTYPLIE_ALL =
            list -> list.stream()
                    .reduce((x,y) -> x * y)
                    .orElse(0);
}

