package ru.omsu.frmnet.projects.functional;

@FunctionalInterface
public interface MyFunction<V, R> {
    R apply(V v);
}
