package ru.omsu.frmnet.projects.functional.twelfth;

import ru.omsu.frmnet.projects.functional.MyFunction;

import java.util.Optional;
import java.util.function.Supplier;

public class FunctionalService {

        private static final int MAX_DEEP = 2;

        public Person getMothersMotherFather(Person person, int deep) {

            if(person == null) {
                return null;
            }

            if(deep == MAX_DEEP) {
                return person.getFather();
            }

            return getMothersMotherFather(person.getMother(), ++deep);
        }

}
