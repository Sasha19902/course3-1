package ru.omsu.frmnet.projects.functional.twelfth;

public class Person {

    private Person mother;
    private Person father;

    public Person(Person mother, Person father) {
        this.mother = mother;
        this.father = father;
    }

    public Person getMother() {
        return mother;
    }

    public void setMother(Person mother) {
        this.mother = mother;
    }

    public Person getFather() {
        return father;
    }

    public void setFather(Person father) {
        this.father = father;
    }
}
