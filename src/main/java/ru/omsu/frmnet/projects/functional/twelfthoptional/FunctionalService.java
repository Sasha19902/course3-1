package ru.omsu.frmnet.projects.functional.twelfthoptional;

import java.util.Optional;
import java.util.function.Function;

public class FunctionalService {

    public Optional<Person> getMothersMotherFather(Optional<Person> person) {
        return person.flatMap(Person::getMother).flatMap(Person::getMother).flatMap(Person::getFather);
    }
}
