package ru.omsu.frmnet.projects.functional.twelfthoptional;

import java.util.Optional;

public class Person {

    private Optional<Person> mother;
    private Optional<Person> father;

    public Person(Person mother, Person father) {
        this.mother = Optional.ofNullable(mother);
        this.father = Optional.ofNullable(father);

    }

    public Optional<Person> getMother() {
        return mother;
    }

    public void setMother(Optional<Person> mother) {
        this.mother = mother;
    }

    public Optional<Person> getFather() {
        return father;
    }

    public void setFather(Optional<Person> father) {
        this.father = father;
    }
}
