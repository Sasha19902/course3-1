package ru.omsu.frmnet.projects.multithread.eighth;

import java.util.concurrent.Semaphore;

public class Main {

    private static final Semaphore WRITE = new Semaphore(0);
    private static final Semaphore READ = new Semaphore(1);

    public static void main(String[] args) {
        Buffer buffer = new Buffer();
        Writer writer = new Writer(buffer);
        Reader reader = new Reader(buffer);

        new Thread(writer).start();
        new Thread(reader).start();
    }



    static class Writer implements Runnable {

        private static final int MAX_COUNT = 10;
        private Buffer buffer;

        public Writer(Buffer buffer) {
            this.buffer = buffer;
        }

        @Override
        public void run() {
            while (true) {
                while (buffer.bf >= MAX_COUNT) {
                    try {
                        WRITE.acquire();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                buffer.write();
                System.out.println("��������� " + buffer.bf);
                READ.release();
            }
        }
    }

    static class Reader implements Runnable {

        private Buffer buffer;

        public Reader(Buffer buffer) {
            this.buffer = buffer;
        }

        @Override
        public void run() {
            while (true) {
                while (buffer.bf == 0) {
                    try {
                        READ.acquire();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                buffer.read();
                System.out.println("������ " + buffer.bf);
                WRITE.release();
            }
        }
    }

    static class Buffer {

        private int bf;

        public Buffer() {
            bf = 0;
        }

        public void write() {
            bf++;
        }

        public void read() {
            bf--;
        }

        public int getProduct() {
            return bf;
        }
    }
}
