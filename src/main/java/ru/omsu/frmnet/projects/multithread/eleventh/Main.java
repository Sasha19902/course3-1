package ru.omsu.frmnet.projects.multithread.eleventh;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Main {

    private static final Lock LOCK = new ReentrantLock();
    private static final Condition PRINTED_PING = LOCK.newCondition();
    private static final Condition PRINTED_PONG = LOCK.newCondition();

    public static void main(String[] args) {
        Ping ping = new Ping(new PingPong());
        Pong pong = new Pong(new PingPong());

        new Thread(ping).start();
        new Thread(pong).start();
    }

    static class Ping implements Runnable {

        private PingPong pingPong;

        public Ping(PingPong pingPong) {
            this.pingPong = pingPong;
        }

        @Override
        public void run() {
            while (true) {
                LOCK.lock();
                try {
                    PRINTED_PONG.awaitNanos(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                pingPong.printPing();
                PRINTED_PING.signal();
                LOCK.unlock();
            }
        }
    }

    static class Pong implements Runnable {

        private PingPong pingPong;

        public Pong(PingPong pingPong) {
            this.pingPong = pingPong;
        }

        @Override
        public void run() {
            while (true) {
                LOCK.lock();
                try {
                    PRINTED_PING.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                pingPong.printPong();
                PRINTED_PONG.signal();
                LOCK.unlock();
            }
        }
    }

    static class PingPong {
        private final String PING = "PING";
        private final String PONG = "PONG";

        public void printPing() {
            System.out.println(PING);
        }

        public void printPong() {
            System.out.println(PONG);
        }
    }
}
