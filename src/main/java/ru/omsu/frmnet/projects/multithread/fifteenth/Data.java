package ru.omsu.frmnet.projects.multithread.fifteenth;

public class Data {

    private int[] intArray;

    public Data(int[] intArray) {
        this.intArray = intArray;
    }

    public int[] getIntArray() {
        return intArray;
    }

    public boolean identificate() {
        return intArray != null;
    }
}
