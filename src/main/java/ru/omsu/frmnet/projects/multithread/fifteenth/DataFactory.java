package ru.omsu.frmnet.projects.multithread.fifteenth;

import java.util.Random;

public class DataFactory {

    private static final int UPPER_BOUND = 25;

    public Data generateData() {
        Random random = new Random();
        int[] temp = new int[random.nextInt(UPPER_BOUND)];

        for(int i = 0; i < temp.length; i++) {
            temp[i] = random.nextInt(UPPER_BOUND);
        }

        return new Data(temp);
    }

}
