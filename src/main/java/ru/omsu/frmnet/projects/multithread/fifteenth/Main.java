package ru.omsu.frmnet.projects.multithread.fifteenth;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Main {

    public static void main(String[] args) throws InterruptedException {

        BlockingQueue<Data> blockingQueue = new LinkedBlockingQueue<>();
        ThreadWriter threadWriter1 = new ThreadWriter(blockingQueue);
        ThreadWriter threadWriter2 = new ThreadWriter(blockingQueue);
        ThreadWriter threadWriter3 = new ThreadWriter(blockingQueue);

        threadWriter1.start();
        threadWriter2.start();
        threadWriter3.start();

        ThreadReader threadReader1 = new ThreadReader(blockingQueue);
        ThreadReader threadReader2 = new ThreadReader(blockingQueue);
        ThreadReader threadReader3 = new ThreadReader(blockingQueue);
        ThreadReader threadReader4 = new ThreadReader(blockingQueue);

        threadReader1.start();
        threadReader2.start();
        threadReader3.start();
        threadReader4.start();

        threadWriter1.join();
        threadWriter2.join();
        threadWriter3.join();

        for(int i = 0; i < 4; i++) {
            blockingQueue.put(new Data(null));
        }
    }
}
