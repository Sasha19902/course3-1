package ru.omsu.frmnet.projects.multithread.fifteenth;

import java.util.Arrays;
import java.util.concurrent.BlockingQueue;

public class ThreadReader extends Thread {

    private BlockingQueue<Data> blockingQueue;

    public ThreadReader(BlockingQueue<Data> blockingQueue) {
        this.blockingQueue = blockingQueue;
    }

    @Override
    public void run() {
        Data temp = null;
        boolean reading = true;
        try {
            temp = blockingQueue.take();
            reading = temp.identificate();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        while (reading) {
            try {
                System.out.println(Arrays.toString(temp.getIntArray()));
                temp = blockingQueue.take();
                reading = temp.identificate();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
