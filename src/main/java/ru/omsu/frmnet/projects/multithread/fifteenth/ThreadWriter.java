package ru.omsu.frmnet.projects.multithread.fifteenth;

import java.util.Random;
import java.util.concurrent.BlockingQueue;

public class ThreadWriter extends Thread {

    protected final static int UPPER_BOUND = 100;
    private final int count;
    private BlockingQueue<Data> blockingQueue;

    public ThreadWriter(BlockingQueue<Data> blockingQueue) {
        count = new Random().nextInt(UPPER_BOUND);
        this.blockingQueue = blockingQueue;
    }

    @Override
    public void run() {
        DataFactory dataFactory = new DataFactory();

        for(int i = 0; i < count; i++) {
            try {
                blockingQueue.put(dataFactory.generateData());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
