package ru.omsu.frmnet.projects.multithread.fifth;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        List<Integer> integers = new ArrayList<>();
        PutterAndGetter putterAndGetter = new PutterAndGetter();
        MyThead putter = new MyThead(integers, "putter", putterAndGetter);
        MyThead getter = new MyThead(integers, "getter", putterAndGetter);

        putter.start();
        getter.start();

        putter.join();
        getter.join();

        System.out.println("SIZE SPISKA - " + integers.size());
    }

    static class MyThead extends Thread {

        private static final int COUNT = 10000;
        private List<Integer> integers;
        private String name;
        private PutterAndGetter putterAndGetter;

        public MyThead(List<Integer> integers, String name, PutterAndGetter putterAndGetter) {
            this.integers = integers;
            this.name = name;
            this.putterAndGetter = putterAndGetter;
        }

        @Override
        public void run() {
            if (name.equals("putter")) {
                put();
            }

            if (name.equals("getter")) {
                get();
            }
        }

        public void put() {
            for (int i = 0; i < COUNT; i++) {
                putterAndGetter.putAndGet(name, integers, i);
            }
        }

        public void get() {
            AtomicInteger atomicInteger = new AtomicInteger(0);


            while (true) {
                if (!integers.isEmpty()) {
                    putterAndGetter.putAndGet(name, integers, atomicInteger.get());
                    atomicInteger.incrementAndGet();
                }
                if (atomicInteger.get() == COUNT) {
                    break;
                }
            }
        }
    }

    static class PutterAndGetter {

        void put(List<Integer> integers, int iteration) {
            Random random = new Random();
            int buffer = random.nextInt(100);
            integers.add(buffer);
            System.out.println("ITERACIA" + iteration + " KLADEM " + buffer);
        }

        void get(List<Integer> integers, int iteration) {
            Random random = new Random();
            int buffer = integers.remove(random.nextInt(integers.size()));
            System.out.println("ITERACIA" + iteration + " ZABIRAEM " + buffer);
        }

        synchronized void putAndGet(String name, List<Integer> integers, int iteration) {
            if (name.equals("putter")) {
                put(integers, iteration);
            }

            if (name.equals("getter")) {
                get(integers, iteration);
            }
        }
    }
}

