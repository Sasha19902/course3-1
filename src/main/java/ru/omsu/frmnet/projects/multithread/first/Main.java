package ru.omsu.frmnet.projects.multithread.first;

public class Main {

    public static void printThreadState() {
        System.out.println(Thread.currentThread());
    }

    public static void main(String[] args) {
        printThreadState();
    }

}
