package ru.omsu.frmnet.projects.multithread.fourth;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();

        Putter putter = new Putter(list);
        Getter getter = new Getter(list);

        putter.start();
        getter.start();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(list.size());
    }

    static class Putter extends Thread {

        private List<Integer> integers;
        private static final int COUNT = 10000;

        Putter(List<Integer> integers) {
            this.integers = integers;
        }


        @Override
        public void run() {
            Random random = new Random();
            int buffer = 0;
            for(int i = 0; i < COUNT; i++) {
                synchronized (integers) {
                    buffer = random.nextInt();
                    integers.add(buffer);
                    System.out.println(buffer + " ДОБАВЛЕН " + i);
                }
            }
        }
    }

    static class Getter extends Thread {

        private List<Integer> integers;
        private static final int COUNT = 10000;

        Getter(List<Integer> integers) {
            this.integers = integers;
        }

        @Override
        public void run() {
            Random random = new Random();
            int iterations = 0;

            while (true) {
                synchronized (integers) {
                    if(!integers.isEmpty()) {
                        System.out.println(integers.remove(random.nextInt(integers.size())) + " УДАЛЁН");
                        iterations++;
                    }
                    if(iterations == COUNT) {
                        break;
                    }
                }
            }
        }
    }
}

