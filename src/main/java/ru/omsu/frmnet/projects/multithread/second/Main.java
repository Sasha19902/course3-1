package ru.omsu.frmnet.projects.multithread.second;

public class Main {

    public static void main(String[] args) {
        MyThread myThread = new MyThread();
        myThread.start();

        try {
            myThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("MyThread stop working");
    }

    static class MyThread extends Thread {

        @Override
        public void run() {
            System.out.println("I start working");
        }
    }

}

