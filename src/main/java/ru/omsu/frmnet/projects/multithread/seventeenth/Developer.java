package ru.omsu.frmnet.projects.multithread.seventeenth;

import java.util.Random;
import java.util.concurrent.BlockingQueue;

public class Developer extends Thread {

    private static final int UPPER_BOUND = 100;
    private final int count;
    private BlockingQueue<Task> blockingQueue;

    public Developer(BlockingQueue<Task> blockingQueue) {
        this.blockingQueue = blockingQueue;
        this.count = new Random().nextInt(UPPER_BOUND);
    }

    @Override
    public void run() {
        TaskFactory taskFactory = new TaskFactory();
        for(int i = 0; i < count; i++) {
            try {
                blockingQueue.put(taskFactory.generateTask());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
