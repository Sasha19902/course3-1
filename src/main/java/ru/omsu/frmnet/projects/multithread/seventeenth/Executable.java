package ru.omsu.frmnet.projects.multithread.seventeenth;

public interface Executable {
    void execute();
}
