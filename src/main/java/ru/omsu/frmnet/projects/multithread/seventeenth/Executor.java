package ru.omsu.frmnet.projects.multithread.seventeenth;

import java.util.concurrent.BlockingQueue;

public class Executor extends Thread {

    private BlockingQueue<Task> blockingQueue;

    public Executor(BlockingQueue<Task> blockingQueue) {
        this.blockingQueue = blockingQueue;
    }

    @Override
    public void run() {
        boolean pushedTask = true;
        boolean identificator = true;
        Task temp = null;

        while (pushedTask || identificator) {
            try {
                temp = blockingQueue.take();
                identificator = temp.identificate();
                temp.execute();
                pushedTask = !temp.end();

                if(pushedTask) {
                    blockingQueue.put(temp);
                }

                if(!pushedTask && !identificator) {
                    blockingQueue.put(temp);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
