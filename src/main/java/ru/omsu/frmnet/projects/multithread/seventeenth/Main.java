package ru.omsu.frmnet.projects.multithread.seventeenth;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        BlockingQueue<Task> blockingQueue = new LinkedBlockingQueue<>();
        Developer developer1 = new Developer(blockingQueue);
        Developer developer2 = new Developer(blockingQueue);
        Developer developer3 = new Developer(blockingQueue);

        Observer observer = new Observer(blockingQueue);
        observer.start();

        Executor executor1 = new Executor(blockingQueue);
        Executor executor2 = new Executor(blockingQueue);
        Executor executor3 = new Executor(blockingQueue);

        developer1.start();
        developer2.start();
        developer3.start();

        executor1.start();
        executor2.start();
        executor3.start();

        developer1.join();
        developer2.join();
        developer3.join();

        SystemExitTask systemExitTask = new SystemExitTask(blockingQueue);
        blockingQueue.put(systemExitTask);

        Thread.sleep(500);
        systemExitTask.drop();
        System.out.println(blockingQueue.size());

    }
}
