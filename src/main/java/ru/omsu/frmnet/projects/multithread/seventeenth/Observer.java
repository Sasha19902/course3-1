package ru.omsu.frmnet.projects.multithread.seventeenth;

import java.util.concurrent.BlockingQueue;

public class Observer extends Thread {

    private BlockingQueue<Task> blockingQueue;

    public Observer(BlockingQueue<Task> blockingQueue) {
        this.blockingQueue = blockingQueue;
    }

    @Override
    public void run() {
        boolean exit = false;

        while (!exit) {
            for(Task task : blockingQueue) {
                exit = !task.identificate() && !task.end();
                if(task.identificate()) {
                    System.out.println(task);
                }
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
