package ru.omsu.frmnet.projects.multithread.seventeenth;

import java.util.concurrent.BlockingQueue;

public class SystemExitTask extends Task {

    private BlockingQueue<Task> tasks;

    public SystemExitTask(BlockingQueue<Task> blockingQueue) {
        super(SYSTEM_EXIT);
        this.tasks = blockingQueue;
    }

    @Override
    public void execute() {
        end = tasks.isEmpty();
    }

    public void drop() {
        try {
            tasks.take();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
