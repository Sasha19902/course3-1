package ru.omsu.frmnet.projects.multithread.seventeenth;

import java.util.*;

public class Task implements Executable {

    protected static final String SYSTEM_EXIT = "EXT";
    protected static final String DEFAULT_P = "DEF";

    private String type;
    private List<Executable> steps;
    private int stage;
    protected boolean end;

    public Task(String type) {
        this.type = type;
    }

    public Task(Executable ... executables) {
        this(DEFAULT_P);
        stage = 0;
        steps = new ArrayList<>(Arrays.asList(executables));
    }

    public boolean end() {
        return end;
    }

    @Override
    public void execute() {
        steps.get(stage++).execute();
        end = stage == steps.size();
    }

    public boolean identificate() {
        return !type.equals(SYSTEM_EXIT);
    }

    @Override
    public String toString() {
        return "TASK: stage " + (stage + 1)+ "/" + steps.size();
    }
}
