package ru.omsu.frmnet.projects.multithread.seventeenth;

import java.util.Random;
import java.util.UUID;

public class TaskFactory {

    private static final int UPPER_BOUND_EX = 15;

    public Task generateTask() {
        Random rand = new Random();
        int countEx = rand.nextInt(UPPER_BOUND_EX) + 1;
        Executable[] executable = new Executable[countEx];

        for(int i = 0; i < countEx; i++) {
            executable[i] = () -> {
                System.out.println("EXECUTE " + UUID.randomUUID().toString());
            };
        }
        return new Task(executable);
    }
}
