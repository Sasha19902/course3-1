package ru.omsu.frmnet.projects.multithread.seventh;

import java.util.concurrent.Semaphore;

public class MainSemaphore {

    private static final Semaphore PING = new Semaphore(0);
    private static final Semaphore PONG = new Semaphore(1);

    public static void main(String[] args) {
        Ping ping = new Ping();
        Pong pong = new Pong();

        ping.start();
        pong.start();
    }

    static class Ping extends Thread {

        @Override
        public void run() {
            ping();
        }

        public void ping() {
            while (true) {
                try {
                    PONG.acquire();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("PING");
                PING.release();
            }
        }
    }

    static class Pong extends Thread {

        @Override
        public void run() {
            pong();
        }

        public void pong() {
            while (true) {
                try {
                    PING.acquire();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("PONG");
                PONG.release();
            }
        }
    }
}
