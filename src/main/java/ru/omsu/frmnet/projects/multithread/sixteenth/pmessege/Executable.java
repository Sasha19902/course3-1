package ru.omsu.frmnet.projects.multithread.sixteenth.pmessege;

public interface Executable extends Identificatable {
    void execute();
}
