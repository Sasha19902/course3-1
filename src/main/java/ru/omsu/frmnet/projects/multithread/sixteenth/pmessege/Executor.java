package ru.omsu.frmnet.projects.multithread.sixteenth.pmessege;

import java.util.concurrent.BlockingQueue;

public class Executor extends Thread {

    private BlockingQueue<Task> blockingQueue;

    public Executor(BlockingQueue<Task> blockingQueue) {
        this.blockingQueue = blockingQueue;
    }

    @Override
    public void run() {
        boolean reading = true;
        Task temp = null;

        while (reading) {
            try {
                temp = blockingQueue.take();
                reading = temp.identificate();
                temp.execute();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
