package ru.omsu.frmnet.projects.multithread.sixteenth.pmessege;

public interface Identificatable {
    boolean identificate();
}
