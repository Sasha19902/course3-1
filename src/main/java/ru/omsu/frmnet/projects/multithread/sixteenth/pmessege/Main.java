package ru.omsu.frmnet.projects.multithread.sixteenth.pmessege;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        BlockingQueue<Task> blockingQueue = new LinkedBlockingQueue<>();
        Developer developer1 = new Developer(blockingQueue);
        Developer developer2 = new Developer(blockingQueue);
        Developer developer3 = new Developer(blockingQueue);

        developer1.start();
        developer2.start();
        developer3.start();

        Executor executor1 = new Executor(blockingQueue);
        Executor executor2 = new Executor(blockingQueue);
        Executor executor3 = new Executor(blockingQueue);
        Executor executor4 = new Executor(blockingQueue);

        executor1.start();
        executor2.start();
        executor3.start();
        executor4.start();

        developer1.join();
        developer2.join();
        developer3.join();

        for(int i = 0; i < 4; i++) {
            blockingQueue.put(new Task(Task.BSERIAL));
        }

    }
}
