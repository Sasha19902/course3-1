package ru.omsu.frmnet.projects.multithread.sixteenth.pmessege;

public class Task implements Executable {

    protected static final String DEFAULT = "DEF";
    protected static final String BSERIAL = "STP";
    private String identificator;

    public Task(String identificator) {
        this.identificator = identificator;
    }

    @Override
    public void execute() {
        System.out.println("EXECUTE " + identificator);
    }

    @Override
    public boolean identificate() {
        return !identificator.equals(BSERIAL);
    }
}
