package ru.omsu.frmnet.projects.multithread.sixth;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        List<Integer> integers = Collections.synchronizedList(new ArrayList<>());
        MyThead putter = new MyThead(integers, "putter");
        MyThead getter = new MyThead(integers, "getter");

        putter.start();
        getter.start();

        putter.join();
        getter.join();

        System.out.println("LIST SIZE " + integers.size());
    }


    static class MyThead extends Thread {

        private static final int COUNT = 10000;
        private List<Integer> integers;
        private String name;

        public MyThead(List<Integer> integers, String name) {
            this.integers = integers;
            this.name = name;
        }

        @Override
        public void run() {
            if (name.equals("putter")) {
                put();
            }

            if (name.equals("getter")) {
                get();
            }
        }

        public void put() {
            Random random = new Random();

            for (int i = 0; i < COUNT; i++) {
                int buffer = random.nextInt(100);
                integers.add(buffer);
                System.out.println("KLADEM " + buffer);
            }
        }

        public void get() {
            AtomicInteger atomicInteger = new AtomicInteger(0);
            Random random = new Random();
            int buffer = 0;

            while (true) {
                if (!integers.isEmpty()) {
                    buffer = integers.remove(random.nextInt(integers.size()));
                    System.out.println("ZABIRAEM " + buffer);
                    atomicInteger.incrementAndGet();
                }
                if (atomicInteger.get() == COUNT) {
                    break;
                }
            }
        }
    }
}
