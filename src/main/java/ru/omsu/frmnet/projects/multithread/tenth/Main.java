package ru.omsu.frmnet.projects.multithread.tenth;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Main {

    public static void main(String[] args) {
        ListIntegers listIntegers = new ListIntegers();

        Putter putter = new Putter(listIntegers);
        Getter getter = new Getter(listIntegers);

        putter.start();
        getter.start();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    static class Putter extends Thread {

        private ListIntegers listIntegers;
        private static final int COUNT = 10000;

        Putter(ListIntegers listIntegers) {
            this.listIntegers = listIntegers;
        }

        @Override
        public void run() {
            Random random = new Random();
            for(int i = 0; i < COUNT; i++) {
                listIntegers.put(random.nextInt(100));
            }
        }
    }

    static class Getter extends Thread {

        private ListIntegers listIntegers;
        private static final int COUNT = 10000;

        Getter(ListIntegers listIntegers) {
            this.listIntegers = listIntegers;
        }
        @Override
        public void run() {
            for(int i = 0; i < COUNT; i++) {
                try {
                    System.out.println(listIntegers.get());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    static class ListIntegers {
        private static final Lock LOCK = new ReentrantLock();
        private static final Condition NOT_EMPTY = LOCK.newCondition();
        private List<Integer> integers;

        public ListIntegers() {
            integers = new ArrayList<>();
        }

        public void put(int element) {
            LOCK.lock();
            integers.add(element);
            NOT_EMPTY.signal();
            LOCK.unlock();
        }

        public int get() throws InterruptedException {
            LOCK.lock();
            try {
                while (integers.isEmpty()) {
                    NOT_EMPTY.await();
                }
                Random random = new Random();
                return integers.remove(random.nextInt(integers.size()));
            } finally {
                LOCK.unlock();
            }
        }

        public boolean isEmpty() {
            return integers.isEmpty();
        }
    }
}

