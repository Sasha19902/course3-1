package ru.omsu.frmnet.projects.multithread.third;

public class Main {


    public static void main(String[] args) {
        FirstThread firstThread = new FirstThread();
        SecondThread secondThread = new SecondThread();
        ThirdThread thirdThread = new ThirdThread();

        firstThread.start();
        secondThread.start();
        thirdThread.start();

        try {
            firstThread.join();
            secondThread.join();
            thirdThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Потоки закончили свою работу");
    }

    static class FirstThread extends Thread {
        @Override
        public void run() {
            System.out.println("First thread start working");
        }
    }

    static class SecondThread extends Thread {
        @Override
        public void run() {
            System.out.println("Second thread start working");
        }
    }

    static class ThirdThread extends Thread {
        @Override
        public void run() {
            System.out.println("Third thread start working");
        }
    }
}

