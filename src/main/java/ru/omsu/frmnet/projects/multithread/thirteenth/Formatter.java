package ru.omsu.frmnet.projects.multithread.thirteenth;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

class Formatter {

    private ThreadLocal<SimpleDateFormat> threadLocal;
    private SimpleDateFormat dateFormat;
    private int i;

    Formatter() {
        this.dateFormat = new SimpleDateFormat();
        i = 1;
        threadLocal = new ThreadLocal<>();
    }

    void format(Date date) {
        threadLocal.set(dateFormat);
        String res = threadLocal.get().format(date);
        System.out.println(i + " date: " + res);
        i++;
    }

}
