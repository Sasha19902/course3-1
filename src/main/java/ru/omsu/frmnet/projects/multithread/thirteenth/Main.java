package ru.omsu.frmnet.projects.multithread.thirteenth;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Main {

    public static void main(String[] args) {
        Formatter formatter = new Formatter();
        FirstThread firstThread = new FirstThread(formatter);
        SecondThread secondThread = new SecondThread(formatter);
        ThirdThread thirdThread = new ThirdThread(formatter);
        FourthThread fourthThread = new FourthThread(formatter);
        FifthThread fifthThread = new FifthThread(formatter);

        firstThread.start();
        secondThread.start();
        thirdThread.start();
        fourthThread.start();
        fifthThread.start();
    }

}

class FirstThread extends Thread {

    Formatter formatter;

    public FirstThread(Formatter formatter) {
        this.formatter = formatter;
    }

    @Override
    public void run() {
        formatter.format(new Date());
    }

}

class SecondThread extends Thread {

    Formatter formatter;

    public SecondThread(Formatter formatter) {
        this.formatter = formatter;
    }

    @Override
    public void run() {
        formatter.format(new Date(1999, 7, 25));
    }

}

class ThirdThread extends Thread {

    Formatter formatter;

    public ThirdThread(Formatter formatter) {
        this.formatter = formatter;
    }

    @Override
    public void run() {
        formatter.format(new Date(1, 2, 3, 4, 5));
    }

}

class FourthThread extends Thread {

    Formatter formatter;

    public FourthThread(Formatter formatter) {
        this.formatter = formatter;
    }

    @Override
    public void run() {
        formatter.format(new Date(1, 2, 3, 4, 6));
    }

}

class FifthThread extends Thread {

    Formatter formatter;

    public FifthThread(Formatter formatter) {
        this.formatter = formatter;
    }

    @Override
    public void run() {
        formatter.format(new Date(100));
    }

}