package ru.omsu.frmnet.projects.multithread.twelfth;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;

public class MyConcurrentHashMap<K, V> extends AbstractMap<K, V>
        implements ConcurrentMap<K, V>, Serializable {


    private ReadWriteLock lock = new ReentrantReadWriteLock();
    private Map<K, V> map;

    public MyConcurrentHashMap() {
        map = new HashMap<>();
        map = Collections.synchronizedMap(map);
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        lock.readLock().lock();
        try {
            return map.entrySet();
        } finally {
            lock.readLock().unlock();
        }
    }

    @Override
    public V getOrDefault(Object key, V defaultValue) {
        lock.readLock().lock();
        try {
            return map.getOrDefault(key, defaultValue);
        } finally {
            lock.readLock().unlock();
        }
    }

    @Override
    public void forEach(BiConsumer<? super K, ? super V> action) {
        lock.writeLock().lock();
        try {
            map.forEach(action);
        } finally {
            lock.writeLock().unlock();
        }
    }

    @Override
    public boolean remove(Object key, Object value) {
        lock.readLock().lock();
        lock.writeLock().lock();
        try {
            return map.remove(key, value);
        } finally {
            lock.writeLock().unlock();
            lock.readLock().unlock();
        }
    }

    @Override
    public boolean replace(K key, V oldValue, V newValue) {
        lock.readLock().lock();
        lock.writeLock().lock();
        try {
            return map.replace(key, oldValue, newValue);
        } finally {
            lock.writeLock();
            lock.readLock();
        }
    }

    @Override
    public void replaceAll(BiFunction<? super K, ? super V, ? extends V> function) {
        lock.writeLock().lock();
        try {
            map.replaceAll(function);
        } finally {
            lock.writeLock().unlock();
        }
    }

    @Override
    public V computeIfAbsent(K key, Function<? super K, ? extends V> mappingFunction) {
        lock.readLock().lock();
        lock.writeLock().lock();
        try {
            return map.computeIfAbsent(key, mappingFunction);
        } finally {
            lock.writeLock().unlock();
            lock.readLock().unlock();
        }
    }

    @Override
    public V computeIfPresent(K key, BiFunction<? super K, ? super V, ? extends V> remappingFunction) {
        lock.readLock().lock();
        lock.writeLock().lock();
        try {
            return map.computeIfPresent(key, remappingFunction);
        } finally {
            lock.writeLock().unlock();
            lock.readLock().unlock();
        }
    }

    @Override
    public V compute(K key, BiFunction<? super K, ? super V, ? extends V> remappingFunction) {
        lock.readLock().lock();
        lock.writeLock().lock();
        try {
            return map.compute(key, remappingFunction);
        } finally {
            lock.writeLock().unlock();
            lock.readLock().unlock();
        }
    }

    @Override
    public V merge(K key, V value, BiFunction<? super V, ? super V, ? extends V> remappingFunction) {
        lock.readLock().lock();
        lock.writeLock().lock();
        try {
            return map.merge(key, value, remappingFunction);
        } finally {
            lock.writeLock().unlock();
            lock.readLock().unlock();
        }
    }

    @Override
    public V putIfAbsent(K key, V value) {
        lock.readLock().lock();
        lock.writeLock().lock();
        try {
            return map.putIfAbsent(key, value);
        } finally {
            lock.writeLock().unlock();
            lock.readLock().unlock();
        }
    }

    @Override
    public V replace(K key, V value) {
        lock.readLock().lock();
        lock.writeLock().lock();
        try {
            return map.replace(key, value);
        } finally {
            lock.writeLock().unlock();
            lock.readLock().unlock();
        }
    }
}
