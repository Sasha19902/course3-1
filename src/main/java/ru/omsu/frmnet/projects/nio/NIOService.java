package ru.omsu.frmnet.projects.nio;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.lang.reflect.Array;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.ByteChannel;
import java.nio.channels.FileChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.function.Function;

public class NIOService {

    private static final int FIRST_NAME_KEY = 0;
    private static final int LAST_NAME_KEY = 1;
    private static final int RATING_KEY = 2;

    private static Trainee createTraineeByString(String traineeInform) throws TrainingException {
        String[] parsed = traineeInform.split(" ");
        return new Trainee(parsed[FIRST_NAME_KEY], parsed[LAST_NAME_KEY], Integer.parseInt(parsed[RATING_KEY]));
    }

    public static Trainee readTraineeByByteBuffer(Path path) throws IOException, TrainingException {
        try(SeekableByteChannel seekableByteChannel = Files.newByteChannel(path)){
            ByteBuffer byteBuffer = ByteBuffer.allocate((int)seekableByteChannel.size());
            seekableByteChannel.read(byteBuffer);
            byteBuffer.rewind();

            return createTraineeByString(new String(byteBuffer.array(), "UTF-8"));
        }
    }

    public static Trainee readTraineeByMappedByteBuffer(Path path) throws IOException, TrainingException {
        CharBuffer charBuffer = null;
        String charEncoding = null;

        try(FileChannel fileChannel = (FileChannel) Files.newByteChannel(path)) {
            MappedByteBuffer mappedByteBuffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, 0, fileChannel.size());
            charEncoding = System.getProperty("file.encoding");
            charBuffer = Charset.forName(charEncoding).decode(mappedByteBuffer);

            return createTraineeByString(charBuffer.toString());
        }
    }

    private static final int ELEM_COUNT = 100;
    private static final int BUFFER_INT_SIZE = Integer.BYTES * ELEM_COUNT;

    public static void writeNumbersByMappedByteBuffer(Path path) throws IOException {
        try(FileChannel fileChannel = (FileChannel) Files.newByteChannel(path,StandardOpenOption.READ,
                StandardOpenOption.WRITE, StandardOpenOption.CREATE)) {
            MappedByteBuffer mappedByteBuffer = fileChannel.map(FileChannel.MapMode.READ_WRITE, 0, BUFFER_INT_SIZE);

            for(int i = 0; i < fileChannel.size() / Integer.BYTES; i++){
                mappedByteBuffer.putInt(i);
            }
        }
    }

    public static ByteBuffer serializeTraineeByteBuffer(Trainee trainee) throws IOException {

        try(ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream)){

            objectOutputStream.writeObject(trainee);
            return ByteBuffer.wrap(byteArrayOutputStream.toByteArray());
        }
    }

    private static final String DAT_FORMAT = ".dat";
    private static final String BAT_FORMAT = ".bat";

    public static void filesDatToBin(Path path) throws IOException {
        Files.walk(path)
             .filter(file -> file.toString().endsWith(DAT_FORMAT))
             .forEach(file -> {
                 try {
                     Files.move(file, file.resolveSibling(file.toString().replace(DAT_FORMAT, BAT_FORMAT)));
                 } catch (IOException e) {
                     e.printStackTrace();
                 }
             });
    }
}
