package ru.omsu.frmnet.projects.nio;

public enum TrainingErrorCode {

    TRAINEE_WRONG_FIRSTNAME("TRAINEE_EXCEPTION: wrong first name"),
    TRAINEE_WRONG_LASTNAME("TRAINEE_EXCEPTION: wrong last name"),
    TRAINEE_WRONG_RATING("TRAINEE_EXCEPTION: wrong rating");

    private String errorCode;

    private TrainingErrorCode(String errorCode){
        this.errorCode = errorCode;
    }

    public String getErrorCode() {
        return errorCode;
    }
}
