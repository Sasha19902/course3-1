package ru.omsu.frmnet.projects.file;

import org.junit.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TestFileService {

    private static final String DEF_NAMES_FILE_TYPES_TEST[] =
            { "file1.txt", "file2.txt", "file3.txt", "lamp.txt", "coal.txt", "fire.txt" };

    private static final String DEF_TEST_NAME_DIRECTORY = "test";

    private static final String DEF_NAME_DIRECTORY = "directory";

    private static final String DEF_NAMES_FILE_TYPES[] =
            { "file1.txt", "file2.txt", "file3.txt", "lamp.txt", "coal.txt", "fire.txt" };

    @BeforeClass
    public static void createFile() {
        List<File> files = new ArrayList<>();
        File file = new File(DEF_TEST_NAME_DIRECTORY);
        file.mkdir();

        for(int i = 0; i < DEF_NAMES_FILE_TYPES.length; i++) {
            files.add(new File(DEF_TEST_NAME_DIRECTORY + "\\" + DEF_NAMES_FILE_TYPES[i]));
        }

        files.forEach(file1 -> {
            try {
                file1.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

    }


    @AfterClass
    public static void clearFiles() throws FileException {
        List<File> files = new ArrayList<>();

        for(int i = 0; i < DEF_NAMES_FILE_TYPES.length; i++) {
            files.add(new File(DEF_NAME_DIRECTORY + "\\" + DEF_NAMES_FILE_TYPES[i]));
        }
        File file = new File(DEF_NAME_DIRECTORY);

        files.forEach(File::delete);
        file.delete();

        List<File> files1 = new ArrayList<>();

        for(int i = 0; i < DEF_NAMES_FILE_TYPES.length; i++) {
            files1.add(new File(DEF_TEST_NAME_DIRECTORY + "\\" + DEF_NAMES_FILE_TYPES[i]));
        }

        File file1 = new File(DEF_TEST_NAME_DIRECTORY);

        files1.forEach(File::delete);
        file1.delete();
    }



    @Test
    public void testFileCreate() throws IOException, FileException {
        FileService.createDirectory(DEF_NAME_DIRECTORY);
        File fileDirectory = new File(DEF_NAME_DIRECTORY);

        List<File> files = new ArrayList<>();

        for(int i = 0; i < DEF_NAMES_FILE_TYPES.length; i++) {
            files.add(new File(DEF_NAME_DIRECTORY + "\\" + DEF_NAMES_FILE_TYPES[i]));
        }

        files.forEach(file -> {
            try {
                FileService.createFile(file.getPath());
            } catch (FileException | IOException e) {
                e.printStackTrace();
            }
        });


        files.forEach(file -> Assert.assertTrue(file.exists()));
        Assert.assertTrue(fileDirectory.exists());
    }

    @Test
    public void testFilesInDirectory() throws FileException {
        File file = new File(DEF_TEST_NAME_DIRECTORY);

        List<File> files = new ArrayList<>();

        for(int i = 0; i < DEF_NAMES_FILE_TYPES.length; i++) {
            files.add(new File(DEF_NAME_DIRECTORY + "\\" + DEF_NAMES_FILE_TYPES[i]));
        }


        List<String> filesNames = FileService.getFilesInDirectory(file.getPath(), "");
        List<String> expected = Arrays.asList(DEF_NAMES_FILE_TYPES);

        filesNames.sort(String::compareTo);
        expected.sort(String::compareTo);


        Assert.assertEquals(expected, filesNames);
    }
}
