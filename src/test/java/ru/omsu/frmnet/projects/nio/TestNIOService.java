package ru.omsu.frmnet.projects.nio;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.omsu.frmnet.projects.file.FileService;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.Paths;

public class TestNIOService {

    private static final String PATH_TXT_FILE = "file.txt";
    private static final String WRITE_NUMBERS_FILE = "file_numbers.txt";

    @AfterClass
    public static void createFiles() throws IOException {
        File file = new File(PATH_TXT_FILE);
        file.createNewFile();
    }

    @BeforeClass
    public static void deleteFiles() {
        File file = new File(PATH_TXT_FILE);
        File file1 = new File(WRITE_NUMBERS_FILE);
        file.delete();
        file1.delete();
    }

    /*
    * Проблема с русскими буквами
    * */

    @Test
    public void testReadTraineeByByteBuffer() throws TrainingException, IOException {
        Trainee trainee = new Trainee("Sasha", "Kovalev", 2);

        FileService.writeTraineeToTextFileOneLine(new File(PATH_TXT_FILE), trainee);
        Trainee newTrainee = NIOService.readTraineeByByteBuffer(Paths.get(PATH_TXT_FILE));

        Assert.assertEquals(trainee, newTrainee);
    }

    @Test
    public void testReadTraineeByMappedByteBuffer() throws TrainingException, IOException {
        Trainee trainee = new Trainee("Sasha", "Kovalev", 2);

        Trainee newTrainee = NIOService.readTraineeByMappedByteBuffer(Paths.get(PATH_TXT_FILE));

        Assert.assertEquals(trainee, newTrainee);
    }

    @Test
    public void testWriteNumbersFile() throws IOException {
        NIOService.writeNumbersByMappedByteBuffer(Paths.get(WRITE_NUMBERS_FILE));
    }

    @Test
    public void testSerialize() throws TrainingException, IOException, ClassNotFoundException {
        Trainee trainee = new Trainee("Sasha", "Kovalev", 2);
        ByteBuffer byteBuffer = NIOService.serializeTraineeByteBuffer(trainee);
        Trainee newTrainee = (Trainee) new ObjectInputStream(new ByteArrayInputStream(byteBuffer.array())).readObject();

        Assert.assertEquals(trainee, newTrainee);
    }
}
